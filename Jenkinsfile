pipeline {

  // Definición del Pod Agente
  agent {
    kubernetes {
      defaultContainer 'jnlp'
      yaml """
            apiVersion: v1
            kind: Pod
            metadata:
            labels:
              component: ci

            spec:
              serviceAccountName: cd-jenkins
              containers:
              - name: kubectl
                image: gcr.io/cloud-builders/kubectl
                command:
                - cat
                tty: true

              - name: docker
                image: docker
                command: ['docker', 'run', '-p', '80:80', 'httpd:latest']
                resources:
                  requests:
                    cpu: 10m
                    memory: 256Mi
                volumeMounts:
                  - mountPath: /var/run
                    name: docker-sock

              volumes:
                - name: docker-sock
                  hostPath:
                    path: /var/run
            """
    }
  }

  // Variables de entorno y variables globales
  environment {
  APP_NAME = "ranap-api"
  IMAGE = "yoshias/${APP_NAME}"
  // Variables para guardar los tags de las imagenes
  IMAGE_DEV_TAG = "dev.${env.BUILD_NUMBER}"
  IMAGE_PROD_TAG = ''
  // Variable para guardar la imagen
  dockerImage = ''
  }

  //-------------------------------------------------------------------
  // Definición de los pasos a dar
  //-------------------------------------------------------------------
  stages {

    stage('Build Image') {
      steps {
        // Si estamos en la master branch obtenemos la versión mediante el tag en git
        script{
          if (env.BRANCH_NAME == 'master'){
            IMAGE_PROD_TAG = sh(returnStdout: true, script: "git describe --tags --abbrev=0").trim()
            echo IMAGE_PROD_TAG
          }
        }
        // Pasamos al container con docker para buildear la imagen
        container('docker'){
          echo 'Starting to build docker image.'
          script {
            // Según la branch añadimos un tag u otro
            if (env.BRANCH_NAME == 'master'){
              dockerImage = docker.build("${IMAGE}:${IMAGE_PROD_TAG}", "./api")
            }
            else{
              dockerImage = docker.build("${IMAGE}:${IMAGE_DEV_TAG}", "./api")
            }
          }
        }
      }
    }
    
    stage('Push Image') {
      environment {
        // Variable con el nombre de las credenciales para DockerHub
        registryCredential = 'DockerHub'
      }
      steps {
        container('docker'){
          script {
            docker.withRegistry( '', registryCredential ) {
              echo 'Pushing docker image to DockerHub'
              dockerImage.push()
              if (env.BRANCH_NAME == 'master'){
                echo 'Pushing with tag latest'
                dockerImage.push('latest')
              }
            }
          }
        }
      }
    }

    // Deploy en el enterno de desarrollo
    stage('Deploy to Develop') {
      // Developer Branch
      when { branch 'develop' }
      steps {
        container('kubectl') {
          // Create develop namespace if it doesn't exist
          sh("kubectl get ns develop || kubectl create ns develop")
          // Change values from production values to dev values
          sh("sed -i.bak 's#image: ${IMAGE}#image: ${IMAGE}:${IMAGE_DEV_TAG}#' ./objetos_k8s/client_deployment-service.yml")
          sh("sed -i.bak 's#${env.API_URI_PROD}#${env.API_URI_DEV}#' ./objetos_k8s/services/managed_certificate.yml")
          sh("sed -i.bak 's#${env.BIGQUERY_DATASET_PROD}#${env.BIGQUERY_DATASET_DEV}#' ./objetos_k8s/client_deployment-service.yml")
          sh("sed -i.bak 's#value: as-ranap-project#value: ${env.GKE_PROJECT_ID}#' ./objetos_k8s/client_deployment-service.yml")
          sh("sed -i.bak 's#value: NOTIFICATION_AUTH_TOKEN#value: ${env.NOTIFICATION_AUTH_TOKEN}#' ./objetos_k8s/client_deployment-service.yml")

          // Deploy on GKE
          step([$class: 'KubernetesEngineBuilder', namespace: "develop", projectId: env.GKE_PROJECT_ID, clusterName: env.GKE_CLUSTER, zone: env.GKE_ZONE, manifestPattern: 'objetos_k8s/services', credentialsId: env.GKE_CRED_ID, verifyDeployments: false])
          step([$class: 'KubernetesEngineBuilder', namespace: "develop", projectId: env.GKE_PROJECT_ID, clusterName: env.GKE_CLUSTER, zone: env.GKE_ZONE, manifestPattern: 'objetos_k8s/client_deployment-service.yml', credentialsId: env.GKE_CRED_ID, verifyDeployments: true])
          step([$class: 'KubernetesEngineBuilder', namespace: "develop", projectId: env.GKE_PROJECT_ID, clusterName: env.GKE_CLUSTER, zone: env.GKE_ZONE, manifestPattern: 'objetos_k8s/api-hpa.yml', credentialsId: env.GKE_CRED_ID, verifyDeployments: true])
        }
      }
    }

    // Deploy en el enterno de producción
    stage('Deploy to Production') {
      // Developer Branch
      when { branch 'master' }
      steps {
        container('kubectl') {
          // Create production namespace if it doesn't exist
          sh("kubectl get ns production || kubectl create ns production")
          // Change variable values for production values
          sh("sed -i.bak 's#image: ${IMAGE}#image: ${IMAGE}:${IMAGE_PROD_TAG}#' ./objetos_k8s/client_deployment-service.yml")
          sh("sed -i.bak 's#value: as-ranap-project#value: ${env.GKE_PROJECT_ID}#' ./objetos_k8s/client_deployment-service.yml")
          sh("sed -i.bak 's#value: NOTIFICATION_AUTH_TOKEN#value: ${env.NOTIFICATION_AUTH_TOKEN}#' ./objetos_k8s/client_deployment-service.yml")
          // Deploy on GKE
          step([$class: 'KubernetesEngineBuilder', namespace: "production", projectId: env.GKE_PROJECT_ID, clusterName: env.GKE_CLUSTER, zone: env.GKE_ZONE, manifestPattern: 'objetos_k8s/services', credentialsId: env.GKE_CRED_ID, verifyDeployments: false])
          step([$class: 'KubernetesEngineBuilder', namespace: "production", projectId: env.GKE_PROJECT_ID, clusterName: env.GKE_CLUSTER, zone: env.GKE_ZONE, manifestPattern: 'objetos_k8s/client_deployment-service.yml', credentialsId: env.GKE_CRED_ID, verifyDeployments: true])
          step([$class: 'KubernetesEngineBuilder', namespace: "production", projectId: env.GKE_PROJECT_ID, clusterName: env.GKE_CLUSTER, zone: env.GKE_ZONE, manifestPattern: 'objetos_k8s/api-hpa.yml', credentialsId: env.GKE_CRED_ID, verifyDeployments: true])
        }
      }
    }
  }
}