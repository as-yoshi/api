from enum import Enum
from os import environ as env
from typing import Optional, Literal, Union

import firebase_admin
from fastapi import FastAPI, status, Response, HTTPException
from firebase_admin import messaging, initialize_app
from google.cloud import bigquery
from google.oauth2 import service_account
from pydantic import BaseModel
from starlette.responses import RedirectResponse
from unidecode import unidecode

description = """
RANAP API
"""


# ---------------------------------------------------------
# Data Classes
# ---------------------------------------------------------
class Provincia(str, Enum):
    a_coruna = 'A Coruña'
    albacete = 'Albacete'
    alicante = 'Alicante'
    almeria = 'Almería'
    araba = 'Araba'
    asturias = 'Asturias'
    avila = 'Ávila'
    badajoz = 'Badajoz'
    baleares = 'Baleares'
    barcelona = 'Barcelona'
    bizkaia = 'Bizkaia'
    burgos = 'Burgos'
    cantabria = 'Cantabria'
    castellon = 'Castellón'
    ceuta = 'Ceuta'
    ciudad_real = 'Ciudad Real'
    cuenca = 'Cuenca'
    caceres = 'Cáceres'
    cadiz = 'Cádiz'
    cordoba = 'Córdoba'
    gipuzkoa = 'Gipuzkoa'
    girona = 'Girona'
    granada = 'Granada'
    guadalajara = 'Guadalajara'
    huelva = 'Huelva'
    huesca = 'Huesca'
    jaen = 'Jaén'
    la_rioja = 'La Rioja'
    las_palmas = 'Las Palmas'
    leon = 'León'
    lugo = 'Lugo'
    lerida = 'Lérida'
    madrid = 'Madrid'
    melilla = 'Melilla'
    murcia = 'Murcia'
    malaga = 'Málaga'
    navarra = 'Navarra'
    ourense = 'Ourense'
    palencia = 'Palencia'
    pontevedra = 'Pontevedra'
    salamanca = 'Salamanca'
    santa_cruz_de_tenerife = 'Santa Cruz de Tenerife'
    segovia = 'Segovia'
    sevilla = 'Sevilla'
    soria = 'Soria'
    tarragona = 'Tarragona'
    teruel = 'Teruel'
    toledo = 'Toledo'
    valencia = 'Valencia'
    valladolid = 'Valladolid'
    zamora = 'Zamora'
    zaragoza = 'Zaragoza'


class AlertData(BaseModel):
    nombre_emergencia: str
    descripcion_emergencia: str
    gravedad: int
    numero_avisos: int
    fecha_aviso: str
    region_emergencia: Provincia


class PushNotificationData(BaseModel):
    alert_data: AlertData
    auth_token: str


# ---------------------------------------------------------
# API Aplication
# ---------------------------------------------------------

# GLOBAL VARIABLES
database_client: Optional[bigquery.Client] = None

# API Configuration
app = FastAPI(
    title="RANAP",
    description=description,
    version="1.3.4",
    contact={
        "name": "Iker de la Iglesia Martínez",
        "email": "idelaiglesia004@ikasle.ehu.eus"
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)


# API startup and shutdown
@app.on_event("startup")
def startup():
    # Inicializamos Firebase
    initialize_app()

    # Inicializamos la base de datos
    bigquery_credentials = service_account.Credentials.from_service_account_file(env['DATABASE_CREDENTIALS'])
    project_id = env['PROJECT_NAME']

    global database_client
    database_client = bigquery.Client(credentials=bigquery_credentials, project=project_id)


@app.on_event("shutdown")
def shutdown():
    # Cerramos la conexión con Firebase
    firebase_admin.delete_app(firebase_admin.get_app())

    # Cerramos la conexión con BigQuery
    database_client.close()


# ---------------------------------------------------------
# DATA Functions
# ---------------------------------------------------------

# Variable for mapping database columns to API especification names
api_columns = {'titulo': 'nombre_emergencia',
               'descripcion': 'descripcion_emergencia',
               'gravedad': 'gravedad',
               'num_avisos': 'numero_avisos',
               'fecha_y_hora': 'fecha_aviso',
               'provincia': 'region_emergencia'}


async def obtener_listado_alertas():
    """
    Obtiene todas las alertas de la BD

    Returns
    -------
    lista_alertas: list
        Lista de diccionarios (alertas)
    """

    # Obtener todas las alertas
    query = f"SELECT titulo, descripcion, gravedad, num_avisos, fecha_y_hora, provincia " \
            f"FROM `{env['DATASET_NAME']}.Alerta` " \
            f"WHERE titulo IS NOT NULL " \
            f"ORDER BY fecha_y_hora"

    return database_client.query(query).to_dataframe().rename(columns=api_columns).to_dict('records')


async def obtener_alertas_provincia(provincia: Provincia):
    """
    Obtiene todas las alertas de una provincia

    Returns
    -------
    lista_alertas: list
        Lista de diccionarios (alertas)
    """

    # Obtener todas las alertas de {provincia}
    query = f"SELECT titulo, descripcion, gravedad, num_avisos, fecha_y_hora, provincia " \
            f"FROM `{env['DATASET_NAME']}.Alerta` " \
            f"WHERE titulo IS NOT NULL AND provincia='{provincia}' " \
            f"ORDER BY fecha_y_hora"

    return database_client.query(query).to_dataframe().rename(columns=api_columns).to_dict('records')


# ---------------------------------------------------------
# API
# ---------------------------------------------------------

@app.get('/', include_in_schema=False)
def root():
    return RedirectResponse(url='/docs')


# Emergencies
# ---------------------------------------------------------

@app.get('/emergencias', status_code=status.HTTP_200_OK, tags=["Data Fetch"])
async def get_emergencies(response: Response):
    # Añadimos la cabecera requerida por Firebase
    response.headers["Access-Control-Allow-Origin"] = "*"
    # Obtenemos todas las alertas
    return await obtener_listado_alertas()


@app.get('/emergencias/{provincia}', status_code=status.HTTP_200_OK, tags=["Data Fetch"])
async def get_province_emergencies(provincia: Provincia, response: Response):
    # Añadimos la cabecera requerida por Firebase
    response.headers["Access-Control-Allow-Origin"] = "*"
    # Obtenemos las alertas de dicha provincia
    return await obtener_alertas_provincia(provincia)


# FireBase
# ---------------------------------------------------------

@app.post('/{client_token}/alertas/{provincia}', status_code=status.HTTP_202_ACCEPTED, tags=["Notifications"])
def suscribe_user_to_alert(client_token: str, provincia: Union[Provincia, Literal["Todas"]], response: Response):
    # Añadimos la cabecera requerida por Firebase
    response.headers["Access-Control-Allow-Origin"] = "*"
    # Procesamos el nombre de la provincia quitando espacios y tíldes y se suscribe al usuario
    messaging.subscribe_to_topic([client_token], unidecode(provincia.replace(' ', '_')))


@app.post('/alertas/notificar', status_code=status.HTTP_201_CREATED, tags=["Notifications"])
def send_alert(data: PushNotificationData):
    # Comprobamos si la petición contiene las credenciales
    if data.auth_token != env['NOTIFICATION_AUTH_TOKEN']:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)

    # Generamos la notificación de la alerta por el canal de la provincia afectada
    message_province = messaging.Message(
        data={k: f'{v}' for k, v in dict(data.alert_data).items()},
        topic=unidecode(data.alert_data.region_emergencia.replace(' ', '_')),
    )

    # Generamos la notificación de la alerta por el canal general
    message_all = messaging.Message(
        data={k: f'{v}' for k, v in data.alert_data.dict().items()},
        topic="Todas",
    )

    # Se mandan los mensajes
    messaging.send(message_province)
    messaging.send(message_all)
